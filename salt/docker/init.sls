dep-packages:
  pkg.installed:
    - pkgs:
      - apt-transport-https
      - ca-certificates
      - curl
      - gnupg2
      - software-properties-common

import-docker-key:
  cmd.run:
    - name: curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-docker-repo:
  cmd.run:
    - name: add-apt-repository -u "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

docker:
  pkg.installed:
    - name: docker-ce
    {% if pillar['docker_version'] %}
    - version: {{ pillar['docker_version'] }}
    {% endif %}
  service.running:
    - name: docker
    - require:
      - pkg: docker-ce
